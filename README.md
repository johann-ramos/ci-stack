# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* CI full containers stack
* Version 1

### How do I get set up? ###

* Clone the repo
* run docker-compose up --build
* How to run tests

### Services ###

* Gitlab: http://0.0.0.0:10080
* Jenkins: http://0.0.0.0:80
* Artifactory: http://0.0.0.0:8081
* Sonar: http://0.0.0.0:9000

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
